#include <unistd.h>
#include <iostream>
#include <semaphore.h>
#include <pthread.h>
#include <stdlib.h>

#define CANT_SILLAS 3

using namespace std;

void *customersProcess(void *algo);
void *barberProcess(void *algo);

sem_t barber;
sem_t customers;
sem_t accessSeats;
int numberOfFreeSeats = CANT_SILLAS;


int main()
{
    int n1 = 1, n2 = 2, n3 = 3,
	n4 = 4, n5 = 5, n6 = 6,
	n7 = 7, n8 = 8, n9 = 9;

    sem_init(&barber, 0, 0);
    sem_init(&customers, 0, 0);
    sem_init(&accessSeats, 0, 1);

    pthread_t barbero;
    pthread_t cliente1;
    pthread_t cliente2;
    pthread_t cliente3;
    pthread_t cliente4;
    pthread_t cliente5;
    pthread_t cliente6;
    pthread_t cliente7;
    pthread_t cliente8;
    pthread_t cliente9;

    cout << "Barbero: Abro la barberia" << endl;
    cout << "La cantidad total de sillas para esperar turno es: " << CANT_SILLAS << endl << endl;
    pthread_create(&barbero, NULL, barberProcess, NULL);
    
    sleep((unsigned int) (rand() % 3) + 1);
    pthread_create(&cliente1, NULL, customersProcess, &n1);
    sleep((unsigned int) (rand() % 5) + 1);
    pthread_create(&cliente2, NULL, customersProcess, &n2);
    sleep((unsigned int) (rand() % 5) + 1);
    pthread_create(&cliente3, NULL, customersProcess, &n3);
    sleep((unsigned int) (rand() % 5) + 1);
    pthread_create(&cliente4, NULL, customersProcess, &n4);
    sleep((unsigned int) (rand() % 5) + 1);
    pthread_create(&cliente5, NULL, customersProcess, &n5);
    sleep((unsigned int) (rand() % 5) + 1);
    pthread_create(&cliente6, NULL, customersProcess, &n6);
    sleep((unsigned int) (rand() % 5) + 1);
    pthread_create(&cliente7, NULL, customersProcess, &n7);
    sleep((unsigned int) (rand() % 5) + 1);
    pthread_create(&cliente8, NULL, customersProcess, &n8);
    sleep((unsigned int) (rand() % 5) + 1);
    pthread_create(&cliente9, NULL, customersProcess, &n9);

    pthread_join(barbero, NULL);

    sem_destroy(&barber);
    sem_destroy(&customers);
    sem_destroy(&accessSeats);

    return 0;
}

void *customersProcess(void *algo)
{
    int *num = (int *) algo;

    sem_wait(&accessSeats); //P(accessSeats);
	if (numberOfFreeSeats > 0)
	{
	    numberOfFreeSeats--;
	    // Se sienta en una silla
	    cout << "Cliente " << *num << ": " << "Me siento a esperar" << endl;
	    cout << "Sillas libres: " << numberOfFreeSeats << "\n" << endl;
	    sem_post(&customers); //V(customers) // Notifico al barbero
	    sem_post(&accessSeats); //V(accessSeats) // Libera zona critica
	    sem_wait(&barber); //P(barber);
	    // Le corta el pelo (se agrega sleep)
	    cout << "Cliente " << *num << ": " << "Me empiezan a cortar el pelo" << endl;
	    cout << "Sillas libres: " << numberOfFreeSeats << "\n" << endl;
	}
	else
	{
	    sem_post(&accessSeats); //V(accessSeats); // Me voy sin corte
	    cout << "Cliente " << *num << ": " << "Me fui sin que me cortaran el pelo >:(" << "\n" << endl;
	}

    return NULL;
}

void *barberProcess(void *algo)
{
    // Codigo
    while (true)
    {
        sem_wait(&customers); //P(customers);
        sem_wait(&accessSeats); //P(accessSeats);

        if (numberOfFreeSeats == CANT_SILLAS)
            cout << "Barbero: No hay cliente, me voy a dormir." << endl;

        numberOfFreeSeats++;
        sem_post(&barber); //V(barber);
        sem_post(&accessSeats); //V(accessSeats);
        // Cortar pelo (se agrega sleep)
        sleep((unsigned int) (rand() % 5) + 7);
        cout << "Barbero: Termine de cortar" << "\n" << endl;
    }

    return NULL;
}

